// ignore_for_file: import_of_legacy_library_into_null_safe, deprecated_member_use

import 'package:flutter/material.dart';
import 'package:splashscreen/splashscreen.dart';
import 'theme/custom_theme.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Splash Theme",
      theme: CustomTheme.lightTheme,
      home: const Splash2(),
    );
  }
}

class Splash2 extends StatelessWidget {
  const Splash2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SplashScreen(
      seconds: 6,
      navigateAfterSeconds: const SecondScreen(),
      title: const Text(
        "Splash Theme",
        textScaleFactor: 2,
      ),
      image: Image.asset("Images/flutter_ui_dev_logo.png"),
      loadingText: const Text("Loading"),
      photoSize: 100.0,
      loaderColor: Colors.blue,
    );
  }
}

class SecondScreen extends StatelessWidget {
  const SecondScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text("Splash Theme")),
        body: Column(
          children: [
            const SizedBox(height: 32),
            RaisedButton(
              child: const Text("To Show Theme"),
              onPressed: () {},
            )
          ],
        ));
  }
}
