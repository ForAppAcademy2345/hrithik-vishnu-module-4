import 'package:flutter/material.dart';

class CustomTheme {
  static ThemeData get lightTheme {
    return ThemeData(
        primaryColor: Colors.yellowAccent,
        scaffoldBackgroundColor: Colors.cyan,
        fontFamily: "Arial",
        buttonTheme: ButtonThemeData(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
          buttonColor: Colors.lightGreenAccent.shade400,
        ));
  }
}
